import requests
from os import getenv
from flickrapi import FlickrAPI
from datetime import datetime
from json import loads


URL = f"https://api.telegram.org/bot{getenv('BOT_TOKEN')}/"

def send_message(text, chat_id):
    final_text = "This is what I got: " + text
    url = URL + "sendMessage?text={}&chat_id={}".format(final_text, chat_id)
    requests.get(url)


def handler(event = None, context = None):
    event = event or {}
    context = context or {}
    
    if not getenv("FLICKR_API_KEY"):
        raise Exception("Please provide Flickr API key")
    if not getenv("FLICKR_API_SECRET"):
        raise Exception("Please provide Flicker API secret")
    if not getenv("BOT_TOKEN"):
        raise Exception("Please provide bot token")

    flickr = FlickrAPI(
        getenv("FLICKR_API_KEY"),
        getenv("FLICKR_API_SECRET"),
        format="parsed-json",
        store_token=False,  # AWS Lambda does not allow to write to FS. 
    )

    photos = flickr.photos.search(
        min_taken_date=datetime.now(),
        per_page=1,
        page=1,
        content_type=1
    )

    for photo in photos['photos']['photo']:
        link = f"https://live.staticflickr.com/{photo['server']}/{photo['id']}_{photo['secret']}_b.jpg"
        print(link)

        message = loads(event['body'])
        chat_id = message['message']['chat']['id']
        reply = message['message']['text']
        if reply == "show me":
            send_message(link, chat_id)
        else:
            send_message("nothing", chat_id)
    return {
        'statusCode': 200
    }
        
if __name__ == "__main__":
    handler(event={})
